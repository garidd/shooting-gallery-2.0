{
    "id": "8edb515d-923d-4d8c-9e1a-d69bb0ac1b9a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "bg_wood",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 255,
    "bbox_left": 0,
    "bbox_right": 255,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "923042c1-d9a2-47fc-bd17-6234dc87df50",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8edb515d-923d-4d8c-9e1a-d69bb0ac1b9a",
            "compositeImage": {
                "id": "8db9261b-469d-483e-9f85-1dcefbe44e4c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "923042c1-d9a2-47fc-bd17-6234dc87df50",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "50408362-381a-418d-8167-ed1750e22028",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "923042c1-d9a2-47fc-bd17-6234dc87df50",
                    "LayerId": "a149113d-616d-4088-80b0-e6b1f5f21f78"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "a149113d-616d-4088-80b0-e6b1f5f21f78",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8edb515d-923d-4d8c-9e1a-d69bb0ac1b9a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": -24,
    "yorig": 59
}