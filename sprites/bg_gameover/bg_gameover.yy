{
    "id": "ec6c2b77-2f51-46e6-8b0b-5ee3474d10d6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "bg_gameover",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 70,
    "bbox_left": 0,
    "bbox_right": 347,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1c47bf1a-bd5e-4273-8454-cd79ea2636d9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ec6c2b77-2f51-46e6-8b0b-5ee3474d10d6",
            "compositeImage": {
                "id": "3435c866-fa0c-422d-9c73-36ec7b9e805c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1c47bf1a-bd5e-4273-8454-cd79ea2636d9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9fa242bf-1aa1-472b-8aa1-e7a5ce3cc825",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1c47bf1a-bd5e-4273-8454-cd79ea2636d9",
                    "LayerId": "59a27bdb-d32e-4144-9f65-81ec79b4f7b2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 72,
    "layers": [
        {
            "id": "59a27bdb-d32e-4144-9f65-81ec79b4f7b2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ec6c2b77-2f51-46e6-8b0b-5ee3474d10d6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 349,
    "xorig": 174,
    "yorig": 36
}