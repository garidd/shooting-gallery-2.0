{
    "id": "bb19b2d8-e653-410a-a9c8-e722d0004c72",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "bg_duck",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 108,
    "bbox_left": 0,
    "bbox_right": 113,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5b05063a-c353-4c8e-94fd-b9da1d54ee4a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bb19b2d8-e653-410a-a9c8-e722d0004c72",
            "compositeImage": {
                "id": "100a90a9-5625-4fae-91e1-0982c8587e6a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5b05063a-c353-4c8e-94fd-b9da1d54ee4a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "00605ce5-5305-49e2-a9d0-2544a89d997a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5b05063a-c353-4c8e-94fd-b9da1d54ee4a",
                    "LayerId": "a9418a37-e393-4609-a7d5-a3b31963240d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 109,
    "layers": [
        {
            "id": "a9418a37-e393-4609-a7d5-a3b31963240d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bb19b2d8-e653-410a-a9c8-e722d0004c72",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 114,
    "xorig": 57,
    "yorig": 108
}