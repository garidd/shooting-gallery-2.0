{
    "id": "7bbcf6df-5a8d-423b-9e98-009361e718b7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "bg_water",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 767,
    "bbox_left": 0,
    "bbox_right": 131,
    "bbox_top": 568,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6a340cac-5628-4636-9def-e0b748c46f68",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7bbcf6df-5a8d-423b-9e98-009361e718b7",
            "compositeImage": {
                "id": "0f374659-5944-492f-a999-f0bb2dc9e553",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6a340cac-5628-4636-9def-e0b748c46f68",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "249f62cb-592f-48fb-a674-ce4539250939",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6a340cac-5628-4636-9def-e0b748c46f68",
                    "LayerId": "83661b2f-6814-4f2d-8cb4-3fcac6e96d0a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 768,
    "layers": [
        {
            "id": "83661b2f-6814-4f2d-8cb4-3fcac6e96d0a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7bbcf6df-5a8d-423b-9e98-009361e718b7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 132,
    "xorig": 0,
    "yorig": 0
}