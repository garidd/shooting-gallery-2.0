{
    "id": "adae8af5-aa89-4950-951a-78243221c7a5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "bg_crosshair",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 49,
    "bbox_left": 0,
    "bbox_right": 49,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9d92afb7-baca-45b8-8873-aff2e8107750",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "adae8af5-aa89-4950-951a-78243221c7a5",
            "compositeImage": {
                "id": "0fb2b6c8-059a-4e0c-8d3e-a74ed3d1bdbe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9d92afb7-baca-45b8-8873-aff2e8107750",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "457705cc-5727-4859-a311-3d415169ad0b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9d92afb7-baca-45b8-8873-aff2e8107750",
                    "LayerId": "195dd9cd-84d9-4ba2-9179-39eff09a5612"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 50,
    "layers": [
        {
            "id": "195dd9cd-84d9-4ba2-9179-39eff09a5612",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "adae8af5-aa89-4950-951a-78243221c7a5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 50,
    "xorig": 25,
    "yorig": 25
}