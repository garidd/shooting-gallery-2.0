{
    "id": "20959bbe-c92c-4458-9634-a7bd428b3184",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "bg_ammo",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 26,
    "bbox_left": 1,
    "bbox_right": 16,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6309e148-cce4-4df2-bd5c-f030be66cd45",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20959bbe-c92c-4458-9634-a7bd428b3184",
            "compositeImage": {
                "id": "c6a3f099-75bb-417d-a68c-238e8633f33e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6309e148-cce4-4df2-bd5c-f030be66cd45",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "937ec9d5-874f-4b05-bfe2-faee0946308b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6309e148-cce4-4df2-bd5c-f030be66cd45",
                    "LayerId": "871cd004-315c-4646-ad83-3e860ba52855"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 27,
    "layers": [
        {
            "id": "871cd004-315c-4646-ad83-3e860ba52855",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "20959bbe-c92c-4458-9634-a7bd428b3184",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 18,
    "xorig": 9,
    "yorig": 13
}