{
    "id": "090e7a78-b00d-48b3-b1fd-c8aa5ce6f7ab",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "bg_curtain",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 79,
    "bbox_left": 0,
    "bbox_right": 255,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "566e7fd9-39c1-4eb2-9b86-2371434fbeef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "090e7a78-b00d-48b3-b1fd-c8aa5ce6f7ab",
            "compositeImage": {
                "id": "d8ed47f1-dced-4f50-b5e8-1ee37999fdfb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "566e7fd9-39c1-4eb2-9b86-2371434fbeef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a4a4eb6a-493a-4b3c-a29f-daae0437deb2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "566e7fd9-39c1-4eb2-9b86-2371434fbeef",
                    "LayerId": "1dc42471-4157-4bdc-8514-88399faafb19"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 80,
    "layers": [
        {
            "id": "1dc42471-4157-4bdc-8514-88399faafb19",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "090e7a78-b00d-48b3-b1fd-c8aa5ce6f7ab",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 265,
    "yorig": -43
}