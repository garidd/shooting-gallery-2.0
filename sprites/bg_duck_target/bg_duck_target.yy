{
    "id": "5374ca19-da71-4345-85eb-eff3f8d2c3e9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "bg_duck_target",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 93,
    "bbox_left": 34,
    "bbox_right": 72,
    "bbox_top": 56,
    "bboxmode": 2,
    "colkind": 2,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f64133d5-4dc2-46e3-9eb8-e488874a484b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5374ca19-da71-4345-85eb-eff3f8d2c3e9",
            "compositeImage": {
                "id": "5ceb7e85-eab9-433a-b7f5-674b0d41db64",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f64133d5-4dc2-46e3-9eb8-e488874a484b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ba17b4d1-d769-46f2-a102-e24d69ac53c9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f64133d5-4dc2-46e3-9eb8-e488874a484b",
                    "LayerId": "78e30eed-9ceb-44e5-a42f-3f875491f62c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 109,
    "layers": [
        {
            "id": "78e30eed-9ceb-44e5-a42f-3f875491f62c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5374ca19-da71-4345-85eb-eff3f8d2c3e9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 114,
    "xorig": 57,
    "yorig": 108
}