{
    "id": "e89cacce-64ef-44fc-ba64-92b75749594d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "bg_hole",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 29,
    "bbox_left": 0,
    "bbox_right": 29,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6e922606-9e96-4ee4-bf2b-3497f3303a3f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e89cacce-64ef-44fc-ba64-92b75749594d",
            "compositeImage": {
                "id": "1836a00d-20b5-4346-b3dc-44629809b079",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6e922606-9e96-4ee4-bf2b-3497f3303a3f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7bb1968a-25b3-4c21-a619-9ecba63b1ef5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6e922606-9e96-4ee4-bf2b-3497f3303a3f",
                    "LayerId": "eb2ed401-e39c-4886-8aab-ed437016e491"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 30,
    "layers": [
        {
            "id": "eb2ed401-e39c-4886-8aab-ed437016e491",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e89cacce-64ef-44fc-ba64-92b75749594d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 30,
    "xorig": 15,
    "yorig": 15
}