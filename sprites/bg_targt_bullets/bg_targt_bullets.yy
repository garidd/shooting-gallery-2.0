{
    "id": "93b33523-1746-4aff-b441-d418b9ff0b53",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "bg_targt_bullets",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 141,
    "bbox_left": 0,
    "bbox_right": 141,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 2,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b4cba7c8-15bb-44c3-a447-27d3cb26cc4c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "93b33523-1746-4aff-b441-d418b9ff0b53",
            "compositeImage": {
                "id": "8517633c-fd1d-4ae8-b00a-e14a940a52ea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b4cba7c8-15bb-44c3-a447-27d3cb26cc4c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dae7600a-7f2d-43f0-93b9-d18625357251",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b4cba7c8-15bb-44c3-a447-27d3cb26cc4c",
                    "LayerId": "96fe4971-1724-4b4f-9feb-427c80d35b2f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 142,
    "layers": [
        {
            "id": "96fe4971-1724-4b4f-9feb-427c80d35b2f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "93b33523-1746-4aff-b441-d418b9ff0b53",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 142,
    "xorig": 71,
    "yorig": 71
}