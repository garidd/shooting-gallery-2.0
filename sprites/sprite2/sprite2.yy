{
    "id": "476b727f-e112-438e-bfa4-942d26f071eb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0dccd4e5-552b-42db-ac47-eb63c7226a50",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "476b727f-e112-438e-bfa4-942d26f071eb",
            "compositeImage": {
                "id": "bf9ddfe9-afa6-45d8-b33f-df6423f0c013",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0dccd4e5-552b-42db-ac47-eb63c7226a50",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "806bf977-37bb-45be-a78f-5fadadf0451f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0dccd4e5-552b-42db-ac47-eb63c7226a50",
                    "LayerId": "47b02806-0761-48f7-b9c2-4b8d8cbfe680"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "47b02806-0761-48f7-b9c2-4b8d8cbfe680",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "476b727f-e112-438e-bfa4-942d26f071eb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": -4,
    "yorig": 25
}