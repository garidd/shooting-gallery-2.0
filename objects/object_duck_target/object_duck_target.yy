{
    "id": "6aef71ad-8619-4d5d-9c30-a540f93ac7d4",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "object_duck_target",
    "eventList": [
        {
            "id": "3ebb99d2-ff2c-4d91-9196-bf7b62397a4e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "6aef71ad-8619-4d5d-9c30-a540f93ac7d4"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "dc3f1f58-2ab2-42f9-96ff-d1caf072a3c2",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "5374ca19-da71-4345-85eb-eff3f8d2c3e9",
    "visible": true
}