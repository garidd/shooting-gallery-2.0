/// @DnDAction : YoYo Games.Instance Variables.Set_Score
/// @DnDVersion : 1
/// @DnDHash : 3C7951B9
/// @DnDArgument : "score" "50"
/// @DnDArgument : "score_relative" "1"
if(!variable_instance_exists(id, "__dnd_score")) __dnd_score = 0;
__dnd_score += real(50);

/// @DnDAction : YoYo Games.Instances.Destroy_Instance
/// @DnDVersion : 1
/// @DnDHash : 66B6B20F
/// @DnDApplyTo : 6aef71ad-8619-4d5d-9c30-a540f93ac7d4
with(object_duck_target) instance_destroy();