{
    "id": "64cd4075-09b2-48b6-bc25-b3b76cfac34f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "object_hole",
    "eventList": [
        {
            "id": "e2fcc7df-7f9c-4ce2-8ce7-1dbd7c52818f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "64cd4075-09b2-48b6-bc25-b3b76cfac34f"
        },
        {
            "id": "1f2a2bf8-67fc-4a97-ace3-e1b7db04bed8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "64cd4075-09b2-48b6-bc25-b3b76cfac34f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "e89cacce-64ef-44fc-ba64-92b75749594d",
    "visible": true
}