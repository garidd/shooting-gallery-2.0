{
    "id": "4304bd5b-e2bf-4bae-a423-528b3d011c44",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "object_bg_game_over",
    "eventList": [
        {
            "id": "1f0370b9-b43c-427d-85fc-49ad8c6740e2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "4304bd5b-e2bf-4bae-a423-528b3d011c44"
        },
        {
            "id": "1de60f91-b1c6-4870-9b2d-5cd140fc3b55",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "4304bd5b-e2bf-4bae-a423-528b3d011c44"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "ec6c2b77-2f51-46e6-8b0b-5ee3474d10d6",
    "visible": true
}