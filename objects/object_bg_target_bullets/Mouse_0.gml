/// @DnDAction : YoYo Games.Instance Variables.Set_Lives
/// @DnDVersion : 1
/// @DnDHash : 649FDB25
/// @DnDApplyTo : 15f776d8-bfe2-4d55-95f3-929ede4d045f
/// @DnDArgument : "lives" "3"
/// @DnDArgument : "lives_relative" "1"
with(object_bg_target_bullets) {
if(!variable_instance_exists(id, "__dnd_lives")) __dnd_lives = 0;
__dnd_lives += real(3);
}

/// @DnDAction : YoYo Games.Instances.Destroy_Instance
/// @DnDVersion : 1
/// @DnDHash : 1ECE2B40
/// @DnDApplyTo : 15f776d8-bfe2-4d55-95f3-929ede4d045f
with(object_bg_target_bullets) instance_destroy();