{
    "id": "15f776d8-bfe2-4d55-95f3-929ede4d045f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "object_bg_target_bullets",
    "eventList": [
        {
            "id": "c86f86c6-59ac-4947-a411-391afc2bfbc3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "15f776d8-bfe2-4d55-95f3-929ede4d045f"
        },
        {
            "id": "0518c594-c109-4db0-984e-4bb06fd307d3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 6,
            "m_owner": "15f776d8-bfe2-4d55-95f3-929ede4d045f"
        },
        {
            "id": "77553f26-f69a-4c63-a6cb-5afb6880f241",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 7,
            "m_owner": "15f776d8-bfe2-4d55-95f3-929ede4d045f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "93b33523-1746-4aff-b441-d418b9ff0b53",
    "visible": true
}