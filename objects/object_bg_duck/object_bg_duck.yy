{
    "id": "dc3f1f58-2ab2-42f9-96ff-d1caf072a3c2",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "object_bg_duck",
    "eventList": [
        {
            "id": "f525944e-6e0e-4046-b555-0bfea21c1baf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "dc3f1f58-2ab2-42f9-96ff-d1caf072a3c2"
        },
        {
            "id": "491c56ff-4707-4f57-80fb-50e7d63b0926",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "dc3f1f58-2ab2-42f9-96ff-d1caf072a3c2"
        },
        {
            "id": "4e41dcb7-3143-4c49-a264-f6243dd1fbb1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "dc3f1f58-2ab2-42f9-96ff-d1caf072a3c2"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "bb19b2d8-e653-410a-a9c8-e722d0004c72",
    "visible": true
}