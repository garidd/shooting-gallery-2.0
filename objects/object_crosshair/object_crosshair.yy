{
    "id": "df3f5885-b9d6-4eea-94d0-cb35790859ad",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "object_crosshair",
    "eventList": [
        {
            "id": "9879473f-9efa-4532-98b9-610b9452462e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "df3f5885-b9d6-4eea-94d0-cb35790859ad"
        },
        {
            "id": "fa2ae8ec-322b-4acd-bf5f-13e0c560ee2c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 7,
            "m_owner": "df3f5885-b9d6-4eea-94d0-cb35790859ad"
        },
        {
            "id": "1fdb904f-e464-4845-9523-afcda986aa33",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "df3f5885-b9d6-4eea-94d0-cb35790859ad"
        },
        {
            "id": "dfec47d8-9fd3-426c-8814-bcd7663511cc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 53,
            "eventtype": 6,
            "m_owner": "df3f5885-b9d6-4eea-94d0-cb35790859ad"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "adae8af5-aa89-4950-951a-78243221c7a5",
    "visible": true
}