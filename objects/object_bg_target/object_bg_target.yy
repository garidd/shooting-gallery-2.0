{
    "id": "689f0a8b-3811-4dcf-8e80-7d08d3cfe299",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "object_bg_target",
    "eventList": [
        {
            "id": "3badcbb5-7f86-44b9-ae6d-2f86ea1e9182",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "689f0a8b-3811-4dcf-8e80-7d08d3cfe299"
        },
        {
            "id": "0718d131-c97d-4a58-8240-34a17f193a2e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 7,
            "m_owner": "689f0a8b-3811-4dcf-8e80-7d08d3cfe299"
        },
        {
            "id": "3fc9a40d-cec0-4f17-b1d6-f9008facb2fb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "689f0a8b-3811-4dcf-8e80-7d08d3cfe299"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "802e9167-5d46-4ade-aa28-36f38f043ec8",
    "visible": true
}