{
    "id": "f61b7259-7cdb-4b17-b8da-92869804275e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "object_ammo",
    "eventList": [
        {
            "id": "d7a386b2-7b8f-47fc-8226-26d2a3b54d67",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "f61b7259-7cdb-4b17-b8da-92869804275e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "20959bbe-c92c-4458-9634-a7bd428b3184",
    "visible": true
}